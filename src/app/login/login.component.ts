import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../environments/environment';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { RecaptchaErrorParameters } from 'ng-recaptcha';
import Swal from 'sweetalert2';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  siteKey = environment.webSecret;
  formLogin: FormGroup;
  resetCaptcha = false;
  error = false;

  loading = true;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private serviceAuth: AuthService,
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
  ) { }

  ngOnInit(): void {
    
    this.renderer.addClass(this.document.body, 'login');

    this.loadFormLogin();
    setTimeout(() => {
      this.loading = false;
    }, 2000);
  }

  loadFormLogin() {
    this.formLogin = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,3}$')]],
      password: ['', [Validators.required]],
      captcha: ['', [Validators.required]],
      privacidad: ['', [Validators.required]],
    });
  }

  public resolved(captchaResponse: string): void {
    // console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    // console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }

  onSubmit() {

    if (this.formLogin.invalid) {
      this.error = true;
      Swal.fire({
        title: 'Lo sentimos...',
        text: 'Por favor complete todos los campos',
        showConfirmButton: false,
        confirmButtonText: 'Ok',
        showCancelButton: true,
        cancelButtonText: 'Cerrar'
      });
      return false;
    }


    const formData = new FormData();

    formData.append('user', this.formLogin.value.email)
    formData.append('password', this.formLogin.value.password);
    formData.append('captcha', this.formLogin.value.captcha);

    this.serviceAuth.postLogin(formData).subscribe(
      (res: any) => {
        console.log(res.data);

        if (res.data.status === 200) {
          this.error = false;
          sessionStorage.setItem('_TOKEN', res.data.data.tk);
          this.router.navigateByUrl('beneficios');
          return;
        }
        Swal.fire({
          title: 'Lo sentimos...',
          text: res.data.mensaje,
          showConfirmButton: false,
          confirmButtonText: 'Ok',
          showCancelButton: true,
          cancelButtonText: 'Cerrar'
        });
        if (res.data.status === 401) {
          this.formLogin.get('captcha').reset();
        }
        this.error = true;
      },
      (err) => {
        console.log('err :>> ', err);
        this.error = true;
      },
      () => { }
    );
  }


}
