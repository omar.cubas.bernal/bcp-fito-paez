import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private env = environment;
  protected endpoint: string;

  constructor(
    private http: HttpClient) {
    this.endpoint = `${environment.api}/v1`;
  }

  postLogin(data) {
    localStorage.setItem('showVideo', '1');

    return this.http.post(`${this.endpoint}/user/signin`, data);
  }


  getToken() {
    const token = sessionStorage.getItem("_TOKEN");

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(this.env.api + '/v1/user/info', { headers });
  }

  postUserEventLive(formData: FormData) {
    const token = sessionStorage.getItem("_TOKEN");

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.post(this.env.api + '/v1/user-live', formData, { headers });
  }
}
