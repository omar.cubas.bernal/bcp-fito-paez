import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/* Rutas */
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { BeneficiosComponent } from './pages/beneficios/beneficios.component';
import { AgendaComponent } from './pages/agenda/agenda.component';
import { TriviaComponent } from './pages/trivia/trivia.component';
import { TransmisionComponent } from './pages/transmision/transmision.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './login/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'beneficios',
    component: BeneficiosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'agenda',
    component: AgendaComponent,
     canActivate: [AuthGuard]
  },
  {
    path: 'trivia',
    component: TriviaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'transmision',
    component: TransmisionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: 'beneficios',
    pathMatch: 'full'
  }

]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
