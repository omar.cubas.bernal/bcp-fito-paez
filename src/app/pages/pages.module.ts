import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages.routing';
import { InicioComponent } from './inicio/inicio.component';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [
    PagesComponent,
    InicioComponent,    
  ],
  imports: [
    BrowserModule,
    PagesRoutingModule,

    SharedModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

})
export class PagesModule { }
