import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/login/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-beneficios',
  templateUrl: './beneficios.component.html',
  styleUrls: ['./beneficios.component.scss']
})
export class BeneficiosComponent implements OnInit {

  loading = true;
  modal;


  url = '../../../assets/video/saludo.mp4'
  constructor(
    private authService : AuthService,
    public navigate: Router,
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
  ) { }

  ngOnInit(): void {
    this.validarToken();
    if(localStorage.getItem('showVideo') == "1"){
      this.modal = true;
    }
    
    setTimeout(() => {
      this.loading = false;
    }, 2000);

    
  }

  close_modal() {
      this.modal = false;
      localStorage.setItem('showVideo',"2");
  }


  validarToken(){
    this.authService.getToken()
    .subscribe(
      (data:any) => {
        if(data.data.status == 401){
          sessionStorage.clear();
          this.navigate.navigateByUrl('login');
        }
      }

    )
  }

}
