import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/login/services/auth.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  constructor(
    private authService : AuthService
  ) { }

  ngOnInit(): void {
    this.validarToken();
  }

  validarToken(){
    this.authService.getToken()
    .subscribe(
      (data:any) => {
        console.log(data);
      }

      )
  }

}
