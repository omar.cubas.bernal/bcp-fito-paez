import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/login/services/auth.service';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {
  loading: boolean = true;

  constructor(
    private authService : AuthService,
    public navigate: Router,
  ) { }

  ngOnInit(): void {
    this.validarToken();
    setTimeout(() => {
      this.loading = false;
    }, 2000);
  }

  validarToken(){
    this.authService.getToken()
    .subscribe(
      (data:any) => {
        console.log(data);

        if(data.data.status == 401){
          sessionStorage.clear();
          this.navigate.navigateByUrl('login');
        }
      }

    )
  }

}
