import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Trivia {
  private env = environment;
  protected endpoint: string;

  constructor(
    private http: HttpClient
  ) {
    this.endpoint = `${environment.api}/v1`;
   }

  getQuestions(){
    const token = sessionStorage.getItem("_TOKEN");

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(this.endpoint + '/question', { headers });

  }


  getAlternative(question){
    const token = sessionStorage.getItem("_TOKEN");

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(this.endpoint + `/question/${question}/alternative`, { headers });

  }

  postUserQuestion(questions){
    const token = sessionStorage.getItem("_TOKEN");

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.post(this.env.api + '/v1/user-question', questions, { headers });
  }

}
