import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/login/services/auth.service';
import { Trivia } from './services/trivia.service';
import * as moment from 'moment';

@Component({
  selector: 'app-trivia',
  templateUrl: './trivia.component.html',
  styleUrls: ['./trivia.component.scss']
})
export class TriviaComponent implements OnInit {
  question: any = [];
  question2: any = [];
  question3: any = [];
  question4: any = [];

  alternative: any = [];
  alternative2: any = [];
  alternative3: any = [];
  alternative4: any = [];


  status = '';
  step1: boolean = true;
  step2: boolean;
  step3: boolean;

  responseUser: any = [];

  perSession = 1;

  formData = new FormData();
  /* Resultado */
  modal = false;
  result = '';
  countResult = 0;

  loading = true;

  constructor(
    private authService: AuthService,
    private triviaService: Trivia,
    public navigate: Router,
  ) { }

  ngOnInit(): void {
    
    this.validarToken();

    this.triviaService.getQuestions()
    .subscribe( (data1: any) => {
      this.question.push(data1.data.data[0].name);
      this.question2.push(data1.data.data[1].name);
      this.question3.push(data1.data.data[2].name);

      data1.data.data.forEach(element1 => {
          //console.log('ID: ',element.id);
          //console.log('Text: ',element.name);

          this.triviaService.getAlternative(element1.id)
          .subscribe(
            (data: any) => {
              //console.log(data);
              
              data.data.data.forEach(element => {
                //console.log('Alternative :>> ', element);

                switch(element1.id){
                  case 1:
                    this.alternative.push(
                      {
                        question_id: element.question_id,
                        alternative_id: element.id,
                        answer: element.answer,
                        respuesta: element.value,
                        valor: element.answer
                      });
                  break;

                  case 2:
                    this.alternative2.push({question_id: element.question_id, alternative_id: element.id, answer: element.answer, respuesta: element.value, valor: element.answer});
                  break;

                  case 3:
                    this.alternative3.push({question_id: element.question_id, alternative_id: element.id, answer: element.answer, respuesta: element.value, valor: element.answer});
                  break;
                  
                }
                //console.log('Respuesta: ',element.value);
                //console.log('valor: ',element.answer);
              });

              
            },
            (err) => {
              console.log('err :>> ', err);
            },
            () => { }
          )
      });
    },
    (err) => {
      console.log('err :>> ', err);
    },
    () => { }
    )

    setTimeout(() => {
      this.loading = false;
    }, 2000);
  /*
    this.alternative = [
      {
        respuesta: '13 de mayo de 1963',
        valor: false
      },
      {
        respuesta: '7 de mayo de 1963',
        valor: false
      },
      {
        respuesta: '13 de marzo de 1963',
        valor: true
      },
      {
        respuesta: '7 de marzo de 1963',
        valor: false
      }
    ]

    this.alternative2 = [
      {
        respuesta: '5',
        valor: true
      },
      {
        respuesta: '7',
        valor: false
      },
      {
        respuesta: '4',
        valor: false
      },
      {
        respuesta: '2',
        valor: false
      }
    ]

    this.alternative3 = [
      {
        respuesta: '1,000,000',
        valor: false
      },
      {
        respuesta: 'Más de 5,000,000',
        valor: false
      },
      {
        respuesta: 'Más de 2,000,000',
        valor: false
      },
      {
        respuesta: 'Más de 3,000,000',
        valor: true
      }
    ]
*/


  }

  validate_step1(event, valor) {
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
      this.step1 = false;
      this.step2 = true;
    }, 1000);

    var answer = document.getElementById(event.target.id);    

    this.formData.append('data[0][question_id]', answer.dataset.question_id);
    this.formData.append('data[0][alternative_id]', answer.dataset.alternative_id);
    this.formData.append('data[0][answer]', answer.dataset.answer);
    this.formData.append('data[0][date_time]',  moment().format("YYYY-MM-DD, h:mm:ss"));


    this.responseUser.push({
      'data[0][question_id]': answer.dataset.question_id,
      'data[0][alternative_id]': answer.dataset.alternative_id,
      'data[0][answer]': answer.dataset.answer,
      'data[0][date_time]': moment().format("YYYY-MM-DD, h:mm:ss")
    })


    if (valor) {
      answer.classList.add('check');
      answer.classList.add('selected');
      this.countResult += 1;
    } else {
      answer.classList.add('check');
      answer.classList.add('error');
    }

    //console.log(this.responseUser);

    var list: any = document.getElementsByClassName("step-1__alternative");
    for (let item of list) {
      var toBlock = document.getElementById(item.id);

      if (!item.classList.contains('check')) {
        toBlock.style.pointerEvents = 'none';
      }
    }


  }

  validate_step2(event, valor) {
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
      this.step2 = false;
      this.step3 = true;
    }, 1000);

    var answer = document.getElementById(event.target.id);
    this.responseUser.push({
      'data[1][question_id]': answer.dataset.question_id,
      'data[1][alternative_id]': answer.dataset.alternative_id,
      'data[1][answer]': answer.dataset.answer,
      'data[1][date_time]': moment().format("YYYY-MM-DD, h:mm:ss")
    })

    this.formData.append('data[1][question_id]', answer.dataset.question_id);
    this.formData.append('data[1][alternative_id]', answer.dataset.alternative_id);
    this.formData.append('data[1][answer]', answer.dataset.answer);
    this.formData.append('data[1][date_time]',  moment().format("YYYY-MM-DD, h:mm:ss"));

    if (valor) {
      answer.classList.add('check');
      answer.classList.add('selected');
      this.countResult += 1;
    } else {
      answer.classList.add('check');
      answer.classList.add('error');
    }

    //console.log(this.countResult);

    

    var list: any = document.getElementsByClassName("step-1__alternative");
    for (let item of list) {
      var toBlock = document.getElementById(item.id);

      if (!item.classList.contains('check')) {
        toBlock.style.pointerEvents = 'none';
      }
    }


  }

  validate_step3(event, valor) {
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
      this.step2 = false;
      this.step3 = true;
      this.modal = true;

      document.getElementById('modal-result').style.visibility = 'visible';

      this.result = `${this.countResult} de 3`;
    }, 1000);


    var answer = document.getElementById(event.target.id);
    //console.log('valor :>> ', valor);

    this.responseUser.push({
      'data[2][question_id]': answer.dataset.question_id,
      'data[2][alternative_id]': answer.dataset.alternative_id,
      'data[2][answer]': answer.dataset.answer,
      'data[2][date_time]': moment().format("YYYY-MM-DD, h:mm:ss")
    })

    this.formData.append('data[2][question_id]', answer.dataset.question_id);
    this.formData.append('data[2][alternative_id]', answer.dataset.alternative_id);
    this.formData.append('data[2][answer]', answer.dataset.answer);
    this.formData.append('data[2][date_time]',  moment().format("YYYY-MM-DD, h:mm:ss"));


    if (valor) {
      answer.classList.add('check');
      answer.classList.add('selected');
      if (!this.result) {
        this.countResult += 1;
      }
    } else {
      answer.classList.add('check');
      answer.classList.add('error');
    }


    if(this.perSession == 1){

      this.triviaService.postUserQuestion(this.formData)
        .subscribe(
          (data:any) =>{
            this.perSession = 0;
        }
      )

    }
    
    var list: any = document.getElementsByClassName("step-1__alternative");

    
    for (let item of list) {
      var toBlock = document.getElementById(item.id);

      if (!item.classList.contains('check')) {
        toBlock.style.pointerEvents = 'none';
      }
    }


  }


  refresh(): void {
    window.location.reload();
  }

  close_modal() {
    document.getElementById('modal-result').style.visibility = 'hidden';
    this.modal = false;
  }

  validarToken() {
    this.authService.getToken()
      .subscribe(
        (data: any) => {
          if (data.data.status == 401) {
            sessionStorage.clear();
            this.navigate.navigateByUrl('login');
          }
        }

      )
  }


}
