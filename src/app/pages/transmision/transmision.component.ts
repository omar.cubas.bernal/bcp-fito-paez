import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { interval, Subscription } from 'rxjs';
import { AuthService } from 'src/app/login/services/auth.service';

@Component({
  selector: 'app-transmision',
  templateUrl: './transmision.component.html',
  styleUrls: ['./transmision.component.scss'],
})
export class TransmisionComponent implements OnInit {
  chat = false;
  showbtn = true;

  @Input() menu!: boolean;
  @Input() menu_responsive?: boolean;

  // @HostListener('document:mousemove', ['$event'])
  // onMouseMove(e) {
    // this.validarToken();
  // }

  /* loader */
  loading: boolean = false;
  onLive: boolean = false;

  info: any;


  /* Contador */
  subscription: any;

  // public dateNow = new Date();
  // public dDay = new Date('Aug 10 2021 14:40:00');
  dDay: any;
  milliSecondsInASecond = 1000;
  hoursInADay = 24;
  minutesInAnHour = 60;
  SecondsInAMinute = 60;

  timeDifference;
  secondsToDday;
  minutesToDday;
  hoursToDday;
  daysToDday;

  tiempoEjecucion: any;

  duration: any;
  diffTime: any;

  constructor(
    private authService: AuthService,
    private navigate: Router,
  ) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.validarToken();
    this.postUserLive('1');



    setTimeout(() => {
      // var eventTime = moment(this.info.date_time).format(); // Timestamp - Sun, 21 Apr 2013 13:00:00 GMT
      // const currentTime = moment().format(); // Timestamp - Sun, 21 Apr 2013 12:30:00 GMT
      // var diffTime = eventTime - currentTime;

      if (this.minutesToDday <= (Math.floor(this.info.settime / 60)) && this.hoursToDday === 0 && this.daysToDday === 0) {
        console.log('this.info if:>> ', this.info);
        this.timeShow(this.info?.setInterval);
      } else {
        console.log('this.info else:>> ', this.info);
        this.timeShow(this.info?.break_interval);
      }

    }, 2000);

    setTimeout(() => {
      this.loading = false;
    }, 3000);


    // this.subscription = interval(1000).subscribe(
    //   (res) => {
    //     this.getTimeDifference();
    //   }
    // );
    this.subscription = setInterval((res) => {

      this.diffTime = moment(this.info?.date_time).diff(moment().format(), 'milliseconds');
      console.log(this.diffTime);
      this.duration = moment.duration(this.diffTime);

      this.secondsToDday = this.duration.seconds();
      this.minutesToDday = this.duration.minutes();
      this.hoursToDday = this.duration.hours();
      this.daysToDday = this.duration.days();

      if (this.diffTime < 0) {
        this.onLive = true;
      } else {
        this.onLive = false;
      }

      // if (!this.onLive) {
      //   this.loading = true;
      // } else {
      //   this.loading = false;
      // }
      setTimeout(() => {
        this.loading = false;
      }, 2000);

    }, 1000);
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy :>> ');
    // this.subscription.unsubscribe();
    // this.tiempoEjecucion.unsubscribe();
    clearInterval(this.tiempoEjecucion);
    // this.subscription.unsubscribe();
    clearInterval(this.subscription);
    this.postUserLive('2');
  }

  timeShow(time: number) {
    // this.tiempoEjecucion = interval(time * 1000).subscribe(
    //   (res: any) => {
    //     console.log('this.info.setInterval interval:>> ', time);
    //     console.log('res interval :>> ', res * time);
    //     console.log('this.minutesToDday :>> ', this.minutesToDday);
    //     this.validarToken();
    //   }
    // );

    this.tiempoEjecucion = setInterval((res) => {
      console.log('this.info.setInterval interval:>> ', time);
      console.log('this.minutesToDday :>> ', this.minutesToDday);
      this.validarToken();
    }, time * 1000);

  }

  // private getTimeDifference() {
  //   this.timeDifference = this.dDay.getTime() - new Date().getTime();
  //   console.log('getTimeDifference :>> ', Math.floor((this.timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute));
  //   if (this.timeDifference > 0) {
  //     this.onLive = false;
  //     this.allocateTimeUnits(this.timeDifference);
  //   } else {
  //     this.onLive = true;
  //     // this.subscription.unsubscribe();
  //   }
  // }

  // private allocateTimeUnits(timeDifference) {
  //   this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
  //   this.minutesToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
  //   this.hoursToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute) % this.hoursInADay);
  //   this.daysToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute * this.hoursInADay));

  //   if (this.minutesToDday == 0 && this.hoursToDday == 0 && this.daysToDday == 0 && this.secondsToDday == 0) {
  //     this.onLive = true;
  //     this.loading = true;
  //     setTimeout(() => {
  //       this.loading = false;
  //     }, 2000);
  //   }
  // }

  showChat() {
    this.chat = !this.chat;
    this.showbtn = false;
  }

  hideChat() {
    this.chat = !this.chat;
    this.showbtn = true;
  }

  validarToken() {
    this.authService.getToken()
      .subscribe(
        (data: any) => {
          if (data.data.status !== 200) {
            sessionStorage.clear();
            this.navigate.navigateByUrl('login');
            // this.tiempoEjecucion.unsubscribe();
            clearInterval(this.tiempoEjecucion);
            // this.subscription.unsubscribe();
            clearInterval(this.subscription);
            this.postUserLive('2');
            return;
          }
          this.info = data.data.data;
          this.dDay = new Date(this.info.date_time);
          console.log('info :>> ', this.info);
          setTimeout(() => {
            this.loading = false;
          }, 3000);
        },
        (err) => {
          sessionStorage.clear();
          this.navigate.navigateByUrl('login');
          // this.tiempoEjecucion.unsubscribe();
          clearInterval(this.tiempoEjecucion);
          // this.subscription.unsubscribe();
          clearInterval(this.subscription);
          this.postUserLive('2');
          return;
        }

      );
  }



  postUserLive(type_hour: '1' | '2') {
    console.log('type_hour :>> ', type_hour);
    const formData = new FormData();
    formData.append('date_', moment().format('YYYY-MM-DD'));
    formData.append('hour_', moment().format('HH:mm:ss'));
    formData.append('type_hour', type_hour);

    this.authService.postUserEventLive(formData).subscribe(
      (res) => {
        console.log('postUserLive :>> ', res);
      },
      (err) => {
        console.log('postUserLive :>> ', err);
      }
    );
  }

}
