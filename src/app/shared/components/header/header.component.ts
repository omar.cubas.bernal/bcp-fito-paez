import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Navigation, Router } from '@angular/router';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})


export class HeaderComponent implements OnInit {
  pathUrl: string;
  menus: Navigation;
  returnPage: boolean = true;
  showMenu: boolean = false;

  status: boolean = true;

  check = new FormGroup({
    valorCheck: new FormControl()
  });

  @Input() menu!: boolean;

  @Output() pageMenu = new EventEmitter<boolean>();
  menuActive = true;

  hideChat: boolean = false;
  changePositionBtn: boolean;
  constructor(
    public navigate: Router,
    private location: Location,
  ) { }

  ngOnInit(): void {
    if(screen.width < 769){
      this.changePositionBtn = true;
    }else{
      this.changePositionBtn = false;
    }
    this.pathUrl = this.location.path();
  }

  goToPage(url: string) {
    this.navigate.navigateByUrl(url);
    this.pathUrl = `/${url}`;
    this.returnPage = !this.returnPage;
  }

  goToPageHome() {
    this.navigate.navigateByUrl('');
    this.pathUrl = `/`;
    this.returnPage = !this.returnPage;
  }

  menuOpenClose(menu: boolean) {
    this.pageMenu.emit(!menu);
    this.menuActive = !this.menuActive;
    this.hideChat = !this.hideChat;
    this.check.reset();
  }

  SignUp() {
    this.check.reset();
    Swal.fire({
      title: 'Cerrar sesión',
      text: '¿Está seguro que deseas cerrar sesión?',
      showCancelButton: true,
      confirmButtonText: 'Si, Cerrar Sesión',
      cancelButtonText: 'No, mantenerme aquí'
    }).then((result) => {
      if (result.value) {

        sessionStorage.clear();
        this.navigate.navigateByUrl('login');
      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
  }

  goNavigate(ruta: string) {
    this.status = true;
    console.log(ruta, this.status);
    this.check.reset();
    this.navigate.navigateByUrl(ruta);
  }

}
