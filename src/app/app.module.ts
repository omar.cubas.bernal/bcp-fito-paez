import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppComponent } from './app.component';

/* Importaciones */
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app.routing';
import { LoginComponent } from './login/login.component';
import { BeneficiosComponent } from './pages/beneficios/beneficios.component';
import { AgendaComponent } from './pages/agenda/agenda.component';
import { TriviaComponent } from './pages/trivia/trivia.component';
import { TransmisionComponent } from './pages/transmision/transmision.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { PagesModule } from './pages/pages.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,

    BeneficiosComponent,
    AgendaComponent,
    TriviaComponent,
    TransmisionComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    /* Modulos */
    SharedModule,
    PagesModule,
    ReactiveFormsModule,
    RecaptchaFormsModule,
    RecaptchaModule,
    HttpClientModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
